![Build Status](https://gitlab.com/EAL-ITT/19a-itt3-pcb/badges/master/pipeline.svg)


# 19A-ITT3-PCB

weekly plans, resources and other relevant stuff for the PCB course in IT technology 3rd semester autumn.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19a-itt3-pcb/)