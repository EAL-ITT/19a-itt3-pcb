---
Week: 48
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 48 - Advenced PCB theory

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

Participate in Advanced PCB theory lecture  
Using Kicad

### Learning goals

The student can:

* Assemble a PCB according to schematic, bill of material and component placement docs
* Test a PCB according to test specifications

The student has knowledge about:

* The complexity in PCB's

## Deliverables

* PCB assembled
* PCB tested
* Gitlab log updated including notes from Advanced PCB theory lecture and PCB tests

## Schedule Tuesday 2019-11-26

* 08:15 Advanced PCB theory lecture hosted by Ilias Esmati (mandatory)
    * Thermal Management
    * Heat sinks
    * Termal interface materials (TIM)
    * Fans proper PCB layout
    * Reliability and safe performance of components 

* If time:
    * Electrical analogy 
    * Power dissipaton
    * Mean Time Before Failure
    * Derating Power Dissapation

* 15:30 End of day

## Schedule Wednesday 2019-11-27

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Assemble produced PCB

Hopefully you received your PCB's and parts by now.

* Assemble your PCB according to the documentation you produced in week 46 (schematic, component placement, Bill of materials)
* Take pictures of the assembly proces for use in the poster exam

### Exercise 1 - Test assembled PCB's

* Test your assembled PCB according to the simulation you specified in week 44.
* Perform other relevant tests (input, output, testpoints, thermal etc.)

Document the test results in your gitlab log.  
It is important that you document your test in a uniform manner, that is using the same format for decimal results, screenshots of simulations etc. 
The test documentation will be a part of the poster you will make for the exam. 

## Comments
 
Exam is alarmingly close (week 50!), if you are behind this is really the time to start catching up.