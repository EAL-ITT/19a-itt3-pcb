---
Week: 45
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 45 - From Capture to PCB Editor

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

Design rule check, netlist, board layout

### Learning goals
* The student can setup and perform design rule check
* The student knows what a netlist is netlist and how to create one 
* The student can create a board layout and place components
* The student has knowledge about both design and physical layers

## Deliverables

* Components placed
* PCB routed
* Silkscreen layers corrected
* Design checked on paper  

## Schedule Tuesday 2019-11-05

* 08:15 Introduction to the day + Q&A
* 08:30 Status
    * Students tells about the status of their work according to exercises
* 09:00 From Capture to PCB Editor theory
* 10:00 Work on deliverables and exercises
* 15:30 End of day

## Schedule Wednesday 2019-11-06

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Create a Netlist and Place Components

To transfer your schematic and footprints to Orcad PCB editor you have to generate a netlist.
After that you will work with the design, place components and create the board outline.
  
[Video 15 - 18](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)

### Exercise 1 - Route PCB

It's time to route your PCB and make adjustments to the silkscreen layers if needed

[Video 19 - 20](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)

### Exercise 2 - Design sanity check

I always perform a sanity check at this stage by printing my PCB on a piece of paper.  
This enables me to check if the components actually fit the design.  

1. Print the design on paper, make sure you have no scaling in the printout
2. Place all components on the printout to see if they fit and that you are able to solder them.

## Comments

[ultralibrarian](https://www.ultralibrarian.com/)  
[componentsearchengine](http://componentsearchengine.com/)  
[octopart](https://octopart.com/)  
Others: TI, Mouser, Farnell etc.