---
title: 'ITT3 PCB'
subtitle: 'Weekly plans'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'ITT1 project, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the weekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programm for each week of the second semester project.
