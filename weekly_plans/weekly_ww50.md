---
Week: 48
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 50 - Prepare exam + Q&A

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Prepare a poster for the poster presentation
* Prepare a pitch for the poster presentation

### Learning goals

* None at this moment

## Deliverables

* Poster uploaded to gitlab project
* Pitch script uploaded to gitlab project

## Schedule Tuesday 2019-12-10

* 08:15 Introduction to the day
    * Questions about exam
* 09:00 Prepare for exam and hand-in
* 15:30 End of day

## Schedule Wednesday 2019-12-11

**Wiseflow hand-in deadline 17:00**

* 08:15 Prepare for exam and hand-in
* 15:30 End of day

## Hands-on time

## Comments
 
