---
Week: 44
Content:  Project statup
Material: See links in weekly plan
Initials: NISI
---

# Week 44 - Orcad Capture

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Simulation or physical tests performed and documented
* Footprints created and assigned in Capture

### Learning goals
* The student can reason about a given circuit
* The student has knowledge about how to convert datasheet specs to footprints
* The student can create padstacks in OrCad 
* The student can create footprints in OrCad

## Deliverables
* Relevant circuit footprints included in Gitlab project
* Log included in Gitlab project

## Schedule Tuesday 2019-10-29

* 08:15 Introduction to the day
* 08:30 Circuit presentations
    * Everyone tells about their chosen circuit and it's functionality

* 10:00 Capture theory
* 11:00 Exercise work
* 15:30 End of day

## Schedule Wednesday 2019-10-30

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Project log

Create a document where you log your experience about the PCB creation process.  
This is mandatory and will be valuable when you has to prepare a pitch for the exam.

The log shall, as a minimum, for each working day include:

1. Summary of activities
2. Lessons learned
3. Ressources used

### Exercise 1 - Prepare schematic for simulation

In the Norcad workshop last week you did som simulations, if in doubt recap those or use the exercises at [Nordcad student forum](https://www.nordcad.dk/dk/student-forum/exercises-en/exercises-en.htm) to recap

1. Make sure you have selected components with at pspice profile attached. All components in the pspice menu have this.
2. Make a list of simulations you want to perform (input and output as a minimum), note them in your log
3. Perform the simulations and capture the results (simulation profile settings and simulated waveforms) in your log or seperate document.

### Exercise 2 - Assign footprints

Before preapring your schematic for PCB you might want to create a copy of the Schematic. The process of changing a simulation schematic to a schematic with footprints might inable you to run the simulations again. 

Watch the video 
[OrCAD 17.2 PCB Design Tutorial - 04 - Capture: Preparing for Manufacture](https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=4)

1. Gather footprints for your circuit (see ressources in comments)  [Instructions](https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=6) 
2. Examine the footprints to make sure they meet the specs in the datasheet.  You might need to change the footprint? [Instructions](https://youtu.be/s8mYUTix3ao)
3. If you can't find a footprint online you will have to make it yourself [Video 8 - 14](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) 


## Comments

[ultralibrarian](https://www.ultralibrarian.com/)  
[componentsearchengine](http://componentsearchengine.com/)  
[octopart](https://octopart.com/)  
Others: TI, Mouser, Farnell etc.