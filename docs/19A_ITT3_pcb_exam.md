# 19A-ITT1-PCB-EXAM

This document describes the exam and hand-in requirements for the PCB elective course.

### Exam description

The examination will be held on, **December 13th 9:00 – 11:30**.
The exam takes place in A1.18 (E-Lab), Seebladsgade 1, 5000 Odense.

There will be cardboard walls available for the posters.   

Students should be ready to present at 9:00, meaning that posters and other relevant material is prepared before exam start.

The exam is a poster presentation where students present the **process** of designing their PCB as well as a presentation of the **physical product**   

The entire exam has a duration of 2,5 hours where students will present to anyone interested.  
During the exam students will present to a teacher and sensor for maximum 5 minutes.  

Grading is passed/not passed.  
The exam is with internal sensor.  
Contact person for this exam is Nikolaj Simonsen, nisi@ucl.dk

### Hand-in

Students are expected to hand-in:

* Front page with picture of PCB
* Pitch preparation
* Link to gitlab repository

Hand in date **December 11th** on wiseflow