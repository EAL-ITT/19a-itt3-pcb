---
title: 'ITT3 PCB'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'ITT3 PCB, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Weekly plans](https://eal-itt.gitlab.io/19a-itt3-pcb/19A_ITT3_pcb_weekly_plans.pdf)



